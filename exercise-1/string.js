function ucfirst(string){
    try {
        let firstLetter = string[0].toUpperCase();
        string = firstLetter + string.slice(1);
        return(string);
    }
    catch (e) {
        return("");
    }
}

function capitalize(string){
    try{
        string = string.toLowerCase();
        let newString = "";
        let i;
        let words = string.split(' ');
        for(i = 0; i < words.length;i++){

            if(words[i] === "")
                continue;

            if(i !== 0)
                newString += " ";

            newString += words[i][0].toUpperCase();
            newString += words[i].slice(1);

        }
        return(newString)
    }
    catch (e) {
        return("");
    }
}

function camelCase(string){
    try{
        string = string.toLowerCase();
        let regex = /[^a-zA-Z0-9]/g;
        string = string.replace(regex," ")
        let newString = "";
        let i;
        let words = string.split(' ');
        for(i = 0; i < words.length;i++){

            if(words[i] === "")
                continue;

            newString += words[i][0].toUpperCase();
            newString += words[i].slice(1);

        }
        return(newString)
    }
    catch (e) {
        return("");
    }
}
